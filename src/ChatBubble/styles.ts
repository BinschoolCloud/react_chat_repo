export default {
  chatbubbleWrapper: {
    overflow: 'auto',
  },
  chatbubble: {
    backgroundColor: '#0084FF',
    borderRadius: 20,
    marginTop: 1,
    marginRight: 'auto',
    marginBottom: 1,
    marginLeft: 'auto',
    maxWidth: 425,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 14,
    paddingRight: 14,
    width: '-webkit-fit-content',
  },
  chatbubbleOrientationNormal: {
    float: 'right',
  },
  recipientChatbubble: {
    backgroundColor: '#ccc',
  },
  recipientChatbubbleOrientationNormal: {
    float: 'left',
  },
  p: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '300',
    margin: 0,
  },
  p_black: {
		color: 'grey',
		fontSize: 11,
		fontWeight: 'bold',
		margin: 0
	},
	chatbubble_time: {
		backgroundColor: '#FFFFFF',
		width:'100%',
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},
	shwTimeRight: {
		display: 'grid';
		float: 'right';
		clear: 'both';
		marginTop: 10;
	},
	shwTimeLeft: {
		display: 'grid';
		float: 'left';
		clear: 'both';
		marginTop: 10;
	}
};
