import * as React from 'react';
import ChatBubbleProps from './interface';
import styles from './styles';

const defaultBubbleStyles = {
  userBubble: {},
  chatbubble: {},
  text: {},
};

export default class ChatBubble extends React.Component {
  props;

  constructor(props: ChatBubbleProps) {
    super(props);
  }

  public render() {
    const { bubblesCentered } = this.props;
    let { bubbleStyles } = this.props;
    bubbleStyles = bubbleStyles || defaultBubbleStyles;
    const { userBubble, chatbubble, text } = bubbleStyles;

    // message.id 0 is reserved for blue
    const chatBubbleStyles =
      this.props.message.id === 0
        ? {
            ...styles.chatbubble,
            ...bubblesCentered ? {} : styles.chatbubbleOrientationNormal,
            ...chatbubble,
            ...userBubble,
          }
        : {
            ...styles.chatbubble,
            ...styles.recipientChatbubble,
            ...bubblesCentered
              ? {}
              : styles.recipientChatbubbleOrientationNormal,
            ...chatbubble,
            ...userBubble,
          };
		  const shwTime = this.props.message.id === 0
			? {
				...styles.shwTimeRight
			}
			: { ...styles.shwTimeLeft };
			const fltDesign = this.props.message.id === 0
			? {
				...styles.chatbubbleOrientationNormal
			}
			: { ...styles.recipientChatbubbleOrientationNormal };
		return (
      <div
        style= {Object.assign({}, { ...styles.chatbubbleWrapper}, shwTime)}
      >
	   <div style={Object.assign({}, chatBubbleStyles, {...styles.chatbubble_time})}>
          <div style={Object.assign({}, {...styles.p_black,...text}, fltDesign)}>{this.props.message.dateFormat}</div>
        </div>
		
        <div style={chatBubbleStyles}>
          <p style={{ ...styles.p, ...text }}>{this.props.message.message}</p>
        </div>
      </div>
    );
  }
}

export { ChatBubbleProps };
